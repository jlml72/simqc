package harryPlotter;

import math.Complex;

public class ComplexFast implements Complex, Cloneable {

    double real, imag;

    public ComplexFast(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public double real() {
        return real;
    }

    public double imag() {
        return imag;
    }

    @Override
    public double r() {
        return Math.sqrt(real * real + imag * imag);
    }

    @Override
    public ComplexFast conj() {
        return new ComplexFast(real, -imag);
    }

    @Override
    public ComplexFast neg() {
        return new ComplexFast(-real, -imag);
    }

    @Override
    public ComplexFast inv() {
        double norm = real * real + imag * imag;
        return new ComplexFast(real / norm, -imag / norm);
    }

    @Override
    public ComplexFast add(Complex z) {
        Algebraic other = z.algebraic();
        return new ComplexFast(real + other.real(), imag + other.imag());
    }

    public ComplexFast addHere(ComplexFast z) {
        real += z.real;
        imag += z.imag;
        return this;
    }

    @Override
    public ComplexFast sub(Complex z) {
        Algebraic other = z.algebraic();
        return new ComplexFast(real - other.real(), imag - other.imag());
    }

    public ComplexFast subHere(ComplexFast z) {
        real -= z.real;
        imag -= z.imag;
        return this;
    }

    @Override
    public ComplexFast mul(Complex z) {
        Algebraic other = z.algebraic();
        return new ComplexFast(real * other.real() - imag * other.imag(), real * other.imag() + imag * other.real());
    }

    public ComplexFast mulHere(ComplexFast z) {
        double oldReal = real;
        double oZReal = z.real;
        real = real * z.real - imag * z.imag;
        imag = oldReal * z.imag + imag * oZReal;
        return this;
    }

    public ComplexFast powHere() {
        double oldReal = real;
        real = real * real - imag * imag;
        imag = 2 * oldReal * imag;
        return this;
    }

    @Override
    public ComplexFast div(Complex z) {
        Algebraic other = z.algebraic();
        double norm = real * real + imag * imag;
        return new ComplexFast((real * other.real() + imag * other.imag()) / norm, (real * other.imag() - imag * other.real()) / norm);
    }

    public ComplexFast divHere(ComplexFast z) {
        double oldReal = real;
        double oZReal = z.real;
        double norm = real * real + imag * imag;
        real = real * z.real + imag * z.imag / norm;
        imag = oldReal * z.imag - imag * oZReal / norm;
        return this;
    }

    @Override
    public Algebraic algebraic() {
        return new Algebraic(real, imag);
    }

    @Override
    public Polar polar() {
        double r = Math.sqrt(real * real + imag * imag);
        double phi = Math.atan2(imag, real);
        return new Polar(r, phi);
    }

    public ComplexFast clone() {
        return new ComplexFast(real, imag);
    }
}
