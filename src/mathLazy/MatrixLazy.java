package mathLazy;

import java.util.Arrays;
import java.util.function.BiPredicate;
import java.util.stream.IntStream;

@Deprecated
public class MatrixLazy {

    static BiPredicate<MatrixLazy, Cell> row(int row) {
        return (m, c) -> c.index() / m.cols == Math.floorMod(row, m.rows);
    }

    static BiPredicate<MatrixLazy, Cell> rowOf(int index) {
        return (m, c) -> c.index() / m.cols == index / m.cols;
    }

    static BiPredicate<MatrixLazy, Cell> col(int col) {
        return (m, c) -> c.index() % m.cols == Math.floorMod(col, m.cols);
    }

    static BiPredicate<MatrixLazy, Cell> colOf(int index) {
        return (m, c) -> c.index() % m.cols == index % m.cols;
    }


    int rows, cols;
    Cell[] cells;

    public MatrixLazy(int rows, int cols, ComplexLazy... values) {
        this(rows, cols, IntStream.range(rows, cols).mapToObj(i -> new Cell(i, values[i])).toArray(Cell[]::new));
    }

    public MatrixLazy(int rows, int cols, Cell... cells) {
        assert rows * cols == cells.length;
        this.rows = rows;
        this.cols = cols;
        this.cells = cells;
    }

    public MatrixLazy add(MatrixLazy other) {
        assert rows == other.rows && cols == other.cols;
        return new MatrixLazy(rows, cols, IntStream.range(0, cells.length)
                .mapToObj(i -> new Cell(i, cells[i].value.add(other.cells[i].value)))
                .toArray(Cell[]::new));
    }


    @Override
    public String toString() {
        return Arrays.toString(cells);
    }

    record Cell(int index, ComplexLazy value) {

    }

}
