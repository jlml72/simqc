package math;

import mathLazy.ComplexLazy;
import mathLazy.MatrixLazy;

public class Test {
    public static void main(String[] args) {

        Complex[] complexes = new Complex[]{new Complex.Algebraic(0, 0), new Complex.Algebraic(0, 1), new Complex.Algebraic(1, 0), new Complex.Algebraic(1, 1)};

        MatrixLazy m1 = new MatrixLazy(2, 2, new ComplexLazy[]{ComplexLazy.algebraic(0, 0), ComplexLazy.algebraic(0, 1), ComplexLazy.algebraic(1, 0), ComplexLazy.algebraic(1, 1)});
        MatrixLazy m2 = new MatrixLazy(2, 2, new ComplexLazy[]{ComplexLazy.algebraic(5, 5), ComplexLazy.algebraic(5, 5), ComplexLazy.algebraic(5, 5), ComplexLazy.algebraic(5, 5)});

        
        MatrixLazy result = m1.add(m2);

        System.out.println(result);
    }
}
