package harryPlotter;

import processing.core.PApplet;
import processing.event.MouseEvent;
import math.Complex;

import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

public class HarryPlotter extends PApplet {

    private static final double ZOOM_AMOUNT = 1.1;
    private static final double PIXEL_SIZE_AMOUNT = 2;

    private double zoom = 0.006;
    private double xPos = 0;
    private double yPos = 0;

    private boolean useDisplayDensity = false;
    private double pixelSize = 1;
    private boolean parallel = false;
    private int fractalDeepness = (int) (-15 * (Math.log(zoom)));
    private double deepnessFactor = 1;

    private boolean redraw = true;
    private boolean alwaysDraw = false;
    private long lastDrawnMs = 0;

    private final List<ColorFunction> colorFunctions = new ArrayList<>();
    private final List<Discrete> complexFunctions = new ArrayList<>();

    public HarryPlotter() {
        init();
    }

    public HarryPlotter(boolean useDisplayDensity) {
        this.useDisplayDensity = useDisplayDensity;
        init();
    }

    private void init() {
        PApplet.runSketch(new String[]{""}, this);
        colorMode(HSB);
    }

    public void settings() {
        size(1000, 900);
        if (useDisplayDensity) {
            pixelDensity(displayDensity());
            pixelSize = 1.0 / pixelDensity;
        }
    }

    public void setup() {
        colorMode(HSB);
        windowResizable(true);
    }

    public void draw() {
        noStroke();

        int deepness = (int) (deepnessFactor * fractalDeepness);

        if (redraw || alwaysDraw) {
            long nowMs = System.currentTimeMillis();
            background(220);
            windowTitle("                 HarryPlotter Plotter rechnet");

            if (pixelSize == 1.0 / pixelDensity) {
                /*colorFunctions.forEach(func ->
                        IntStream.range(0, width * pixelDensity).parallel().forEach(x ->
                                IntStream.range(0, height * pixelDensity).forEach(y -> {
                                    set(x, y, func.apply(convertToCoordinate(x + 0.5, y + 0.5), fractalDeepness));
                                })
                        )
                );*/
                int denseWidth = width * pixelDensity * pixelDensity;
                int denseHeight = height * pixelDensity * pixelDensity;

                colorFunctions.forEach(func -> {
                    IntStream intStream = IntStream.range(0, denseWidth * denseHeight);
                    if (parallel) intStream = intStream.parallel();
                    intStream.forEach(i -> {
                                    int x = i % denseWidth;
                                    int y = i / denseWidth;
                                    set(x, y, func.apply(convertToCoordinate(x + 0.5 * pixelSize, y + 0.5 * pixelSize, pixelDensity), deepness));
                                });

                });
            } else {
                int denseWidth = (int) ((width + pixelSize - 1) / pixelSize);
                int denseHeight = (int) ((height + pixelSize - 1) / pixelSize);

                colorFunctions.forEach(func -> {
                    IntStream intStream = IntStream.range(0, denseWidth * denseHeight);
                    if (parallel) intStream = intStream.parallel();

                    intStream.forEach(i -> {
                        double x = (i % denseWidth) * pixelSize;
                        double y = (i / denseWidth) * pixelSize;
                        fill(func.apply(convertToCoordinate(x + 0.5 * pixelSize, y + 0.5 * pixelSize), deepness));
                        rect((float) x, (float) y, (float) pixelSize, (float) pixelSize);
                    });
                });
            }


            // Draw Coordinates
            ComplexFast zeroCoordinate = convertToPixel(0, 0);
            float zeroX = (float) zeroCoordinate.real();
            float zeroY = (float) zeroCoordinate.imag();

            // Draw Bars
            // White Part of X-Axis
            if (0 <= zeroY && zeroY < height) {
                stroke(0, 0, 255);
                strokeWeight(2);
                line(0, zeroY, width, zeroY);
            }

            // White Part of Y-Axis
            if (0 <= zeroX && zeroX < width) {
                stroke(0, 0, 255);
                strokeWeight(2);
                line(zeroX, 0, zeroX, height);
            }

            // Black Part of X-Axis
            if (0 <= zeroY && zeroY < height) {
                stroke(0f);
                strokeWeight(1);
                line(0, zeroY, width, zeroY);
            }

            // Black Part of Y-Axis
            if (0 <= zeroX && zeroX < width) {
                stroke(0f);
                strokeWeight(1);
                line(zeroX, 0, zeroX, height);
            }

            // Draw Complex Functions
            complexFunctions.forEach(func -> {

                // Evaluate new Values
                double[] values = lastDrawnMs == 0 ? func.getValues() : func.getNewTimestamp((nowMs - lastDrawnMs) * 0.001);
                //System.out.println("Rotton: " + red(func.fillColor) + " | " + Arrays.stream(values).sum());
                double delta = func.getDelta();

                // Fill if needed
                if ((func.fillColor & 0xFF000000) != 0) fill(func.fillColor);
                else noFill();
                // Format Stroke
                stroke(func.strokeColor);
                strokeWeight(func.strokeWeight);

                // Draw Shape
                beginShape();
                vertex((float) convertXtoPixel(func.xMin), (float) convertYtoPixel(values[0]));
                IntStream.range(1, values.length - 1).forEach(i -> {
                    double x = func.xMin + i * delta;
                    curveVertex((float) convertXtoPixel(x), (float) convertYtoPixel(values[i]));
                });
                vertex((float) convertXtoPixel(func.xMax), (float) convertYtoPixel(values[values.length - 1]));
                endShape();

            });

            // Management
            lastDrawnMs = nowMs;
            windowTitle("HarryPlotter Plotter");
            redraw = false;
        }
    }

    ComplexFast convertToCoordinate(double x, double y, double pixelDensity) {
        return new ComplexFast(zoom * (x - 0.5 * width * pixelDensity) / pixelDensity - xPos, - (zoom * (y - 0.5 * height * pixelDensity) / pixelDensity + yPos));
    }

    ComplexFast convertToCoordinate(double x, double y) {
        return new ComplexFast(zoom * (x - 0.5 * width) - xPos, - (zoom * (y - 0.5 * height) + yPos));
    }

    ComplexFast convertToPixel(double xCoordinate, double yCoordinate) {
        return new ComplexFast(convertXtoPixel(xCoordinate), convertYtoPixel(yCoordinate));
    }

    double convertXtoPixel(double xCoordinate) {
        return (xCoordinate + xPos) / zoom + 0.5 * width;
    }

    double convertYtoPixel(double yCoordinate) {
        return -(yCoordinate + yPos) / zoom + 0.5 * height;
    }

    @Override
    public void keyTyped() {
        if (key == ',') {
            setPixelSize(pixelSize / PIXEL_SIZE_AMOUNT);
            System.out.println("Resized Pixel: " + pixelSize);
        } else if (key == '.') {
            setPixelSize(pixelSize * PIXEL_SIZE_AMOUNT);
            System.out.println("Resized Pixel: " + pixelSize);
        } else if (key == 'p') {
            parallel = !parallel;
            redraw = true;
            System.out.println("Parallele Berechnung: " + parallel);
        } else if (key == 'x') {
            deepnessFactor /= 1.2;
            System.out.println("Maximale Tiefe: " + deepnessFactor * fractalDeepness + (deepnessFactor == 1 ? " (standard)" : ""));
            redraw = true;
        } else if (key == 'y') {
            deepnessFactor *= 1.2;
            System.out.println("Maximale Tiefe: " + deepnessFactor * fractalDeepness + (deepnessFactor == 1 ? " (standard)" : ""));
            redraw = true;
        }
    }

    public void mouseWheel(MouseEvent event) {
        redraw = true;
        move(- mouseX + width / 2, mouseY - height / 2);
        zoom *=  Math.pow(ZOOM_AMOUNT, Math.signum(event.getCount()));
        move(mouseX - width / 2, - mouseY + height / 2);
        fractalDeepness = (int) (-15 * (Math.log(zoom)));
    }

    public void mouseDragged(MouseEvent e) {
        move(e.getX() - pmouseX, -e.getY() + pmouseY);
        redraw = true;
    }

    @Override
    public void windowResized() {
        redraw = true;
    }

    private void move(int x, int y) {
        xPos += x * zoom;
        yPos += y * zoom;
    }


    public HarryPlotter add(ColorFunction colorFunction) {
        colorFunctions.add(colorFunction);
        redraw = true;
        return this;
    }

    public HarryPlotter add(ColorFunction... colorFunctionArray) {
        for (ColorFunction colorFunction : colorFunctionArray) add(colorFunction);
        redraw = true;
        return this;
    }

    public HarryPlotter add(Complex complex) {
        complex.add(complex);
        redraw = true;
        return this;
    }

    public HarryPlotter add(Discrete discrete) {
        complexFunctions.add(discrete);
        alwaysDraw = true;
        return this;
    }

    public HarryPlotter add(Complex... complexArray) {
        for (Complex complex : complexArray) add(complex);
        redraw = true;
        return this;
    }

    public HarryPlotter setPixelSize(double pixelSize) {
        this.pixelSize = (pixelSize > 0) ? pixelSize : 1;
        redraw = true;
        return this;
    }




}
