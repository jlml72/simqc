package math;

import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Matrix {

    static BiPredicate<Matrix, Cell> row(int row) {
        return (m, c) -> c.index() / m.cols == Math.floorMod(row, m.rows);
    }

    static BiPredicate<Matrix, Cell> row(Cell cell) {
        return (m, c) -> c.index() / m.cols == cell.index() / m.cols;
    }

    static BiPredicate<Matrix, Cell> col(int col) {
        return (m, c) -> c.index() % m.cols == Math.floorMod(col, m.cols);
    }

    static BiPredicate<Matrix, Cell> col(Cell cell) {
        return (m, c) -> c.index() % m.cols == cell.index() % m.cols;
    }

    int cols, rows;
    Cell[] cells;

    public Matrix(int cols, int rows, Cell[] cells) {
        this.cols = cols;
        this.rows = rows;
        this.cells = cells;
    }

    public Matrix(int cols, int rows, Complex... values) {
        assert cols * rows == values.length;

        this.cols = cols;
        this.rows = rows;

        final Complex[] complexes = values.clone(); // :/
        this.cells = IntStream.range(0, values.length).mapToObj(i -> new Cell(() -> complexes[i], i)).toArray(Cell[]::new);

    }

    public Stream<Cell> stream() {
        return Arrays.stream(cells);
    }

    public Stream<Cell> stream(BiPredicate<Matrix, Cell> predicate) {
        return stream().filter(c -> predicate.test(this, c));
    }

    public Matrix add(Matrix other) {
        assert cols == other.cols && rows == other.rows : "Bei Addition müssen die Matrizen die gleiche Größe haben";
        return new Matrix(cols, rows, stream().map(c -> new Cell(() -> c.value.supply().add(other.cell(c).value.supply()), c.index)).toArray(Cell[]::new));
        //return new Matrix(cols, rows,IntStream.range(0, cells.length).mapToObj(i -> new Cell(() -> cell(i).value().supply().add(other.cell(i).value().supply()), i)).toArray(Cell[]::new));
    }

    public Cell cell(Cell atOther) {
        return cell(atOther.index);
    }

    public Cell cell(int index) {
        return cells[index];
    }

    @Override
    public String toString() {
        return Arrays.toString(cells);
    }

    record Cell(ComplexSupplier value, int index) {

        public ComplexSupplier value() {
            System.out.println("Value gotten");
            return value;
        }

        @Override
        public String toString() {
            return "" + value.supply();
        }
    }

    interface ComplexSupplier {

        Complex supply();

    }
}
