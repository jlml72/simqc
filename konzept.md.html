<meta charset="utf-8">

**Simulation von Schrödingers Wellengleichung**

Die Gleichung
=======================================================================================================================

Die Wellengleichung von Schrödinger beschreibt das Verhalten eines einzelnen Elektrons in einem Potenzialfeld:

<br>\begin{equation}
    i\frac { \partial }{ \partial t } \psi = -\frac { \partial^2 }{ \partial x^2 } \psi + V(x)\psi
\end{equation}<br>


- $\psi$ (psi) beschreibt den sog. quantenmechanischen Zustand des Elektrons. Da $\psi$ sowohl vom Ort $x$ (eindimensional), als auch von der Zeit $t$ abhängig ist, ist $\psi$ eine Funktion: $\psi(x, t) \in \mathbb{C}$. _Die Funktionsparameter werden oft aus Übersichtlichkeitsgründen weggelassen. Um einheitlich zu bleiben, werde ich diese ebenfalls weglassen._
    - $|\psi(x, t)|^2$ stellt dabei die Wahrscheinlichkeitsdichte dar und wird gerne beim Visualisieren verwendet. Der Flächeninhalt unterhalb des Graphs ist die Wahrscheinlichkeit, das Elektron bei einer Messung zum Zeitpunkt $t$ in einem gewissen Bereich anzutreffen.

- $\frac { \partial }{ \partial t }\psi$, ist $\psi$ abgeleitet nach der Zeit. Der Operator $\frac { \partial }{ \partial t }$ bedeutet, dass $\psi$ nur von der Zeit $t$ (und nicht nach dem Ort) abgeleitet wird. _In einfachen Worten: Die Änderung im Laufe der Zeit._

- $\frac { \partial^2 }{ \partial x^2 }\psi$ ist die zweite Ableitung von $\psi$ nach dem Ort x. Die zweite Ableitung wird durch das "_hoch 2_" gekennzeichnet.

- $V(x)$ beschreibt das Potenzial abhängig vom Ort $x$. _Notiz: In der Simulation von Hugo Martay war_ $V(x) = ax^2$.



Jetzt wird's zeit-diskret
========================================================================================================================

Die Idee
------------------------------------------------------------------------------------------------------------------------

Um die zeit- und ort-kontinuierliche Gleichung mit einem Computer zu simulieren, müssen wir uns von der Kontinuität lösen und alle Werte diskret darstellen.
Zuerst soll die Gleichung zeit-diskret werden. Am einfachsten ist es für die Simulation, wenn es eine Funktion/Methode gibt, die den aktuellen Zustand entgegennimmt und den neuen Zustand für den nächsten Zeitschritt $+\Delta t$ berechnet:
$$
    \psi_{+ \Delta t} =\ ...
$$

Dazu wird die Wellengleichung (1) von oben etwas umgestellt:

$$
    \\
    \begin {array}{rrcll}
        &
        i\frac { \partial }{ \partial t } \psi&
        =&
        -\frac { \partial^2 }{ \partial x^2 } \psi + V(x)\psi&
        |\ \cdot \frac { 1 }{ i }
    \\\\
        \Leftrightarrow&
        \frac { \partial }{ \partial t } \psi&
        =&
        -i\left(-\frac { \partial^2 }{ \partial x^2 } \psi + V(x)\psi \right)&
    \end {array}
$$
_Hinweis:_ $\frac { 1 }{ i } = -i$

Auf der linken Seite steht jetzt nur noch die Ableitung von $\psi$ nach der Zeit.

Jetzt soll die Gleichung zeit-diskret werden. Dazu wird ein $\Delta t$ gewählt, dass den Abstand zwischen zwei Zeitschritten definiert. Da auf der linken Seite jedoch eine Ableitung nach der Zeit steht, wird ein diskretes Ableitungsverfahren benötigt.


Das Verfahren
------------------------------------------------------------------------------------------------------------------------

Bei folgendem Verfahren wird sich zunutze gemacht, dass eine Ableitung f'(t) einer Funktion f am Punkt t, die Steigung an dem Punkt darstellt:

![Die Steigung an einem Punkte wird durch ein gedachtes Dreieck angenähert. (Abtastrate: $\Delta t = 1$)](AbleitungZeit.png width="60%")

Dieses Verfahren nähert die Steigung an einem Punkt an, indem die Steigung zwischen dem Punkt einen Zeitschritt vor ($t - \Delta t$) und einem Zeitschritt nach ($t + \Delta t$) dem betrachteten Punkt errechnet:
$$
    \frac { \Delta y }{ 2 \Delta t } \thickapprox f'(t) \thickapprox \frac { f(t + \Delta t) - f(t - \Delta t) }{ 2 \Delta t }
$$


<details>
    <summary>(Klick) Hugo Martay verwendet eine leicht andere Annäherung.</summary>

    ![Die Steigung zwischen zwei Punkten wird durch ein gedachtes Dreieck angenähert. (Abtastrate: $\Delta t = 1$)](AbleitungZeitHugo.png width="60%")

    Dieses Verfahren nähert die Steigung __zwischen__ zwei Punkten, sodass man folgende Gleichung erhält:
    $$
    \frac { \Delta y }{ \Delta t } \thickapprox f'(t) \thickapprox \frac { f(t + 0.5 \Delta t) - f(t - 0.5 \Delta t) }{ \Delta t }
    $$
    wobei $f'(t)$ nur für Werte zwischen zwei Zeitpunkten, also nur für $t + 0.5 \Delta t$ definiert ist. Somit stellt
    $$
    \Delta y \thickapprox f'(t + 0.5 \Delta t) \thickapprox \frac { f(t + \Delta t) - f(t) }{ \Delta t }
    $$
    die Differenz vom Zeitpunkt $t$ zum nächsten Zeitpunkt $t + \Delta t$ dar.

    Da $\Delta t$ relativ klein gewählt wird, ist folgende Näherung möglich:
    $$
    f'(t + 0.5 \Delta t) = \frac { f(t + \Delta t) - f(t) }{ \Delta t } \thickapprox f'(t)
    $$

    In der Simulation stellt sich diese Variante jedoch als etwas ungenauer heraus. Beide Varianten haben zwar den Schwachpunkt, dass den Graphen "spitze Stacheln" wachsen, jedoch behält meine Variante auch noch nach einiger Zeit einen relativ konstanten Flächeninhalt ein. Die Unterschiede werden sehr deutlich, wenn man größere Zeitschritte ($\Delta t > 0.005$) wählt.

    Im Fo,genden werde ich meine Annäherung verwenden, sodass leichte Unterschiede zu den Formeln von Hugo Martay entstehen.

</details>

Die Anwendung
------------------------------------------------------------------------------------------------------------------------

Nun soll das Verfahren auf $\frac { \partial }{ \partial t } \psi(x, t)$ anwendet werden:
$$
    \frac { \partial }{ \partial t } \psi(x, t) \thickapprox \frac { \psi(x, t + \Delta t) - \psi(x, t - \Delta t) }{ 2 \Delta t }
$$
_Aus Übersichtlichkeitsgründen wird im Folgenden_ $\psi(x, t + \Delta t)$ _gegen_ $\psi_{+ \Delta t}$ _und_ $\psi(x, t - \Delta t)$ _gegen_ $\psi_{- \Delta t}$ _ersetzt._

Nun wird das Ergebnis in die Wellengleichung eingesetzt, sodass folgende Umformung möglich wird:
$$
    \frac { \psi_{+ \Delta t} - \psi_{- \Delta t} }{ 2 \Delta t } \thickapprox -i\left(-\frac { \partial^2 }{ \partial x^2 } \psi + V(x)\psi \right)
    \ \ \ |\ \cdot 2 \Delta t
\\\
\\\
    \psi_{+ \Delta t} - \psi_{- \Delta t}  \thickapprox -i \cdot 2 \Delta t \left(-\frac { \partial^2 }{ \partial x^2 } \psi + V(x)\psi \right)
    \ \ \ |\ + \psi_{- \Delta t}
\\
$$
\begin{equation}
    \psi_{+ \Delta t} \thickapprox \psi_{- \Delta t} -i \cdot 2 \Delta t \left(-\frac { \partial^2 }{ \partial x^2 } \psi + V(x)\psi \right)
\end{equation}


Das letzt, was uns jetzt noch hindert, diese Gleichung als Methode `psiNext(...)` zu implementieren, ist nur noch $\frac { \partial^2 }{ \partial x^2 } \psi$.

Ort-Diskret
------------------------------------------------------------------------------------------------------------------------

Nach dem gleichen Verfahren, wie bei der Ableitung vorhin bilden wir die diskrete Ableitung, indem wir die Steigung zwischen zwei diskreten Punkten berechnen. Diesmal jedoch zwischen zwei aneinanderliegenden Punkten:
$$
    f'(x) \thickapprox \frac { f(x + 0.5 \Delta x) - f(t - 0.5 \Delta x) }{ \Delta x }
$$
_Bemerkung: Mit diesem Verfahren ist die Ableitung nur zwischen zwei Punkten definiert, da sonst $f$ für einen nicht existierenden Punkt aufgerufen werden würde._
Da nun ein allgemeines Verfahren zum Ableiten einer Funktion gegeben ist, kann dieses nun auf die erste Ableitung einer Funktion angewandt werden, um die zweite Ableitung zu bilden.
$$
    \Rightarrow f''(x) \thickapprox \frac { f'(x + 0.5 \Delta x) - f'(x - 0.5 \Delta x) }{ \Delta x }
\\\
\\\
    \Leftrightarrow f''(x) \thickapprox \frac { \frac { f(x + \Delta x) - f(x) }{ \Delta x } - \frac { f(x) - f(x - \Delta x) }{ \Delta x } }{ \Delta x }
\\\
\\\
    \Leftrightarrow f''(x) \thickapprox \frac { f(x + \Delta x) - 2f(x) + f(x - \Delta x) }{ \Delta x^2 }
$$

Somit erhalten wir eine Funktion, die die zweite Ableitung an einem Punkt annähert und dabei genau auf einem Punkt liegt und nicht wie oben zwischen zwei Punkten.

Ähnlich wie oben kann dieses Verfahren nun verwendet werden um $\frac { \partial^2 }{ \partial x^2 } \psi$ zu ersetzen.

$$
    \frac { \partial^2 }{ \partial x^2 } \psi(x, t) \thickapprox \frac { \psi(x + \Delta x, t) - 2\psi(x, t) + \psi(x - \Delta x, t) }{ \Delta x^2 }
$$

Auch hier werde ich ähnlich wie oben $\psi(x + \Delta x, t)$ gegen $\psi_{+ \Delta x}$, usw. ersetzen.

Die diskrete Gleichung
------------------------------------------------------------------------------------------------------------------------

Jetzt müssen wir nur noch die gesammelten Bausteine zusammensetzen um die Gleichung diskret simulieren zu können. Es ergibt sich folgende Gleichung:

$$
\psi_{+ \Delta t} \thickapprox \psi_{- \Delta t} -i \cdot 2 \Delta t \left(-\frac { \psi_{+\Delta x} - 2\psi + \psi_{- \Delta x} }{ \Delta x^2 } + V(x)\psi \right)
$$

\begin{equation}
\Leftrightarrow \  \psi_{+ \Delta t} \thickapprox \psi_{- \Delta t} + i \cdot 2 \Delta t \left(\frac { \psi_{+\Delta x} - 2\psi + \psi_{- \Delta x} }{ \Delta x^2 } - V(x)\psi \right)
\end{equation}

Und in ausführlich:

$$
\psi(x, t + \Delta t) \thickapprox \psi_(x, t - \Delta t) + i \cdot 2 \Delta t \left(\frac { \psi(x + \Delta x, t) - 2\psi(x, t) + \psi(x - \Delta x, t) }{ \Delta x^2 } - V(x) \cdot \psi(x, t) \right)
$$

Diese Gleichung kann nun in der Simulation implementiert werden, um $\psi$ für den nächsten Zeitschritt zu berechnen.

---

_Jannis Müller, V0.1 (2022-11-07)_











<!--<script>markdeepOptions={tocStyle:'none'}</script>-->
<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js" charset="utf-8"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>