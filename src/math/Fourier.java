package math;

import processing.core.PConstants;
import processing.core.PVector;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Fourier {
/*
    public double getFFTdBFS(double frequency) {
        assert frequency > 0 && frequency <= sampleRate * 0.5 : "Die Frequenz muss > 0 sein und darf das Nyquist-Limit nicht übersteigen! (" + frequency + ")";

        //      k = Phasen pro Sample
        //  <=> k = Pufferlänge * SamplesProPhase
        //  <=> k = Pufferlänge * Frequenz / SampleRate
        //
        // vom Typen float, da mit der Klasse PVector gearbeitet wird
        float k = (float) (channelSamples[0].length * frequency / sampleRate);

        // Jeder Kanal wird einzeln analysiert und am Ende der Durchschnitt der Kanäle errechnet
        double fftValue = Arrays.stream(channelSamples)
                .map(samples -> IntStream.range(0, channelSamples[1].length)
                        // Jedes Sample wird als komplexe Zahl mithilfe eines zweidimensionalen PVectors dargestellt.
                        // Der Winkel der Komplexen Zahl entspricht dem Phasenwinkel (deswegen PVector.fromAngle(Phasenwinkel))
                        // und die Länge bzw. der Betrag stellt den Ausschlag des Samples dar.
                        // (Entstandener Vektor der Länge 1 wird mit dem Ausschlag des Samples multipliziert)
                        .mapToObj(i -> PVector.fromAngle(2 * PConstants.PI * k * i / channelSamples[0].length).mult(channelSamples[1][i]))
                        // Alle entstandenen komplexen Werte werden addiert
                        .reduce(new PVector(), (complex1, complex2) -> PVector.add(complex1, complex2)))
                // Die Intensität der Testfrequenz ist nun proportional zum Betrag
                // der komplexen Zahl.
                // (PVector::mag errechnet die Länge des Vektors bzw. den Betrag der komplexen Zahl)
                .mapToDouble(PVector::mag).average().orElse(0);

        // Damit ist der Vorschlag für ein ganzes Abschlussprojekt aus Hr. Herzbergs Projektideen
        // "Frequenzanalyse mit der Diskreten Fourier Transformation"
        // praktisch in einem einzigen (verschachtelten) Stream gelöst ;-)

        // Zum Zurückgeben wird der errechnete Wert noch mit dem Proportionalitätsfaktor
        // verrechnet und in die Einheit dBFS umgerechnet
        return intTOdBFS((int) (HEADROOM_DIVISOR * fftValue * 2.0 / channelSamples[0].length));
    }
    */
}
