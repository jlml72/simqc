package mathLazy;

import java.util.function.DoubleSupplier;

@Deprecated
public class ComplexLazy {

    public static ComplexLazy algebraic(double real, double imag) {
        return algebraic(() -> real, () -> imag);
    }

    public static ComplexLazy algebraic(DoubleSupplier real, DoubleSupplier imag) {
        return new ComplexLazy(Format.ALGEBRAIC, real, imag);
    }

    public static ComplexLazy polar(double r, double phi) {
        return polar(() -> r, () -> phi);
    }

    public static ComplexLazy polar(DoubleSupplier r, DoubleSupplier phi) {
        return new ComplexLazy(Format.ALGEBRAIC, r, phi);
    }


    private final Format format;
    private final DoubleSupplier a, b;


    /*
    ALGEBRAIC:
    a: real
    b: imag

    POLAR:
    a: r
    b: phi
     */
    private ComplexLazy(Format format, DoubleSupplier a, DoubleSupplier b) {
        this.format = format;
        this.a = a;
        this.b = b;
    }

    public double real() {
        return algebraic().a.getAsDouble();
    }

    public double imag() {
        return algebraic().b.getAsDouble();
    }

    public double r() {
        return polar().a.getAsDouble();
    }

    public double phi() {
        return polar().b.getAsDouble();
    }


    public ComplexLazy conj() {
        return algebraic(() -> real(), () -> -imag());
    }

    public ComplexLazy neg() {
        return algebraic(() -> -real(), () -> -imag());
    }

    public ComplexLazy inv() {
        return polar(() -> 1 / r() , () -> -phi());
    }


    public ComplexLazy add(ComplexLazy z) {
        return algebraic(() -> real() + z.real(), () -> imag() + z.imag());
    }

    public ComplexLazy sub(ComplexLazy z) {
        return add(z.neg());
    }

    public ComplexLazy mul(ComplexLazy z) {
        return polar(() -> r() * z.r(), () -> phi() + z.phi());
    }

    public ComplexLazy div(ComplexLazy z) {
        return mul(z.inv());
    }

    private ComplexLazy algebraic() {
        if (format == Format.ALGEBRAIC) return this;
        // else format is Format.POLAR
        return algebraic(
                () -> r() * Math.cos(phi()),
                () -> r() * Math.sin(phi())
        );
    }

    private ComplexLazy polar() {
        if (format == Format.ALGEBRAIC) return this;
        // else format is Format.ALGEBRAIC
        return polar(
                Math.sqrt(real() * real() + imag() * imag()),
                Math.atan2(imag(), real())
        );
    }



    enum Format {
        ALGEBRAIC, POLAR;
    }


    @Override
    public String toString() {
        return "ComplexLazy{" +
                "format=" + format +
                ", a=" + a.getAsDouble() +
                ", b=" + b.getAsDouble() +
                '}';
    }
}