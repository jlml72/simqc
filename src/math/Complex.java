package math;

public interface Complex {

    default double real() {
        return algebraic().real();
    }

    default double imag() {
        return algebraic().imag();
    }

    default double r() {
        return polar().r();
    }

    default double phi() {
        return polar().phi();
    }

    default Complex conj() {
        return new Algebraic(algebraic().real, -algebraic().imag);
    }
    default Complex neg() {
        return new Algebraic(-algebraic().real, -algebraic().imag);
    }
    default Complex inv() {
        return new Polar(1 / polar().r , -polar().phi);
    }


    default Complex add(Complex z) {
        return new Algebraic(algebraic().real + z.algebraic().real, algebraic().imag + z.algebraic().imag);
    }
    default Complex sub(Complex z) {
        return new Algebraic(algebraic().real - z.algebraic().real, algebraic().imag - z.algebraic().imag);
    }
    default Complex mul(Complex z) {
        return new Polar(polar().r * z.polar().r, polar().phi + z.polar().phi);
    }
    default Complex div(Complex z) {
        return new Polar(polar().r / z.polar().r, polar().phi - z.polar().phi);
    }

    // Helpers
    default Complex add(double real, double imag) {
        return add(new Complex.Algebraic(real, imag));
    }
    default Complex sub(double real, double imag) {
        return sub(new Complex.Algebraic(real, imag));
    }
    default Complex mul(double real, double imag) {
        return mul(new Complex.Algebraic(real, imag));
    }
    default Complex div(double real, double imag) {
        return div(new Complex.Algebraic(real, imag));
    }


    default Algebraic algebraic() {
        return (Algebraic) this;
    }
    default Polar polar() {
        return (Polar) this;
    }



    record Algebraic(double real, double imag) implements Complex {
        @Override
        public Polar polar() {
            double r = Math.sqrt(real * real + imag * imag);
            double phi = Math.atan2(imag, real);
            return new Polar(r, phi);
        }
    }

    public record Polar(double r, double phi) implements Complex {
        @Override
        public Algebraic algebraic() {
            double real = Math.cos(phi) * r;
            double imag = Math.sin(phi) * r;
            return new Algebraic(real, imag);
        }
    }


}