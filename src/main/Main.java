package main;

import harryPlotter.Discrete;
import harryPlotter.HarryPlotter;
import math.Complex;

import java.util.function.DoubleUnaryOperator;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        HarryPlotter harryPlotter = new HarryPlotter(true);
        harryPlotter.colorMode(HarryPlotter.HSB);
        Thread.sleep(100);

        // Mandelbrot
        /*harryPlotter.add((complex, deepness) -> {
            int i = 0;
            double abs;
            ComplexFast start = complex.clone();
            ComplexFast current = complex.clone();

            while ((abs = current.abs()) <= 4 && ++i <= deepness) {
                current.mulHere(current).addHere(start);
            }
            float result = i - (float) ((Math.log(Math.log(abs)) / Math.log(2)));
            return harryPlotter.color(50 * (float) (Math.log(result + 5) - Math.log(5)) % 255, 100, 105 * (deepness - result));
        });*/

        // IDEE POTENTIAL ERHÖHEN
        DoubleUnaryOperator potentialFunc = (x) -> x * x * 0.5;//Math.cos(4 * x) * 0.5 + x * x * 0.2;

        /*harryPlotter.add(new Discrete<>(
                // Bounds
                -7, 7,
                // Number of timesteps per frame
                1,
                // Initial Array
                IntStream.range(0, 100).mapToObj(i -> Double.valueOf(0)).toArray(Double[]::new),
                // Mapper: Complex -> Double
                c -> c,
                // Function: Next
                (Discrete.Neighbor<Double>) (lastLeft, last, lastRight, secondLast, xPos, deltaSeconds) -> potentialFunc.applyAsDouble(xPos))
                .withStroke(harryPlotter.color(180f, 120f, 110f), 3)
        );*/

        Discrete.Neighbor<Complex> equation = (lastLeft, last, lastRight, secondLast, xPos, deltaSeconds) -> {
            double potential = potentialFunc.applyAsDouble(xPos);

            // Calculating kinetic Part
            Complex kineticApplied = last.mul(new Complex.Algebraic(2, 0)).sub(lastLeft).sub(lastRight);

            // Calculating potential Part
            Complex potentialApplied = last.mul(new Complex.Algebraic(potential, 0));

            // Merge
            return last.add((kineticApplied.add(potentialApplied)).mul(new Complex.Algebraic(0, -5 * deltaSeconds)));
        };

        var initial = IntStream.range(0, 128*3)
                .mapToObj(i -> new Complex.Algebraic(Math.exp(-(i*0.33 - 32.0) * (i*0.33 - 32.0) / (5 * 5)) * 0.75, 0))
                .toArray(Complex[]::new);


        harryPlotter.add(new Discrete<>(
                        // Bounds
                        -3, 3,
                        // Number of Timesteps per Frame
                        100,
                        // Initial Array
                        initial,
                        // Mapper: Complex -> Double
                        c -> c.algebraic().r(),
                        // Function: Next
                        equation
                )
                .withStroke(harryPlotter.color(250f, 170f, 180f), 3)
                .withFill(harryPlotter.color(250f, 170f, 240f, 80f))
        );


        harryPlotter.add(new Discrete<>(
                        // Bounds
                        -3, 3,
                        // Number of Timesteps per Frame
                        100,
                        // Initial Array
                        initial,
                        // Mapper: Complex -> Double
                        c -> c.algebraic().r(),
                        // Function: Next
                        (Discrete.Neighbor<Complex>) (lastLeft, last, lastRight, secondLast, xPos, deltaSeconds) -> {
                            double potential = potentialFunc.applyAsDouble(xPos);

                            // Calculating kinetic Part
                            Complex kineticApplied = last.mul(new Complex.Algebraic(2, 0)).sub(lastLeft).sub(lastRight);

                            // Calculating potential Part
                            Complex potentialApplied = last.mul(new Complex.Algebraic(potential, 0));

                            // Merge
                            //return (secondLast.mul(new Complex.Algebraic(0.5, 0)).add(last.mul(new Complex.Algebraic(0.5, 0))))
                            return secondLast.add((kineticApplied.add(potentialApplied)).mul(new Complex.Algebraic(0, -10 * deltaSeconds)));
                        }
                )
                        .withStroke(harryPlotter.color(200f, 170f, 180f), 3)
        );

/*
        harryPlotter.add(new Discrete<>(
                        // Bounds
                        -3, 3,
                        // Number of Timesteps per Frame
                        250,
                        // Initial Array
                        initial,
                        // Mapper: Complex -> Double
                        c -> c.algebraic().real(),
                        // Function: Next
                        equation
                )
                .withStroke(harryPlotter.color(90f, 150f, 145f), 3)
        );

        harryPlotter.add(new Discrete<>(
                        // Bounds
                        -3, 3,
                        // Number of Timesteps per Frame
                        250,
                        // Initial Array
                        initial,
                        // Mapper: Complex -> Double
                        c -> c.algebraic().imag(),
                        // Function: Next
                        equation
                )
                .withStroke(harryPlotter.color(150f, 170f, 180f), 3)
        );*/


    }

}
