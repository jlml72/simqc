package harryPlotter;

import java.util.Arrays;
import java.util.function.ToDoubleFunction;
import java.util.stream.IntStream;

public class Discrete<T> {

    final double xMin, xMax;
    final int numTimesteps;
    T[] values;
    T[] lastValues;
    ToDoubleFunction<T> mapper;
    Function<T> function;

    // Color optional
    int fillColor = 0x00000000;
    int strokeColor = 0xFF000000;
    float strokeWeight = 2;

    public Discrete(double xMin, double xMax, int numTimesteps, T[] values, ToDoubleFunction<T> mapper, Function<T> function) {
        assert xMin < xMax;
        assert values.length >= 3;
        this.xMin = xMin;
        this.xMax = xMax;
        this.numTimesteps = numTimesteps;
        this.values = values;
        lastValues = values;
        this.mapper = mapper;
        this.function = function;
    }

    public double[] getNewTimestamp(double deltaSeconds) {
        double deltaTimestep = deltaSeconds / numTimesteps;
        for (int i = 0; i < numTimesteps; i++) {
            T[] newValues = function.apply(values, lastValues, xMin, xMax, deltaTimestep);
            lastValues = values;
            values = newValues;
        }
        return getValues();
    }

    public double[] getValues() {
        return Arrays.stream(values).mapToDouble(mapper).toArray();
    }

    public double getDelta() {
        return (xMax - xMin) / (values.length - 1);
    }

    public Discrete withFill(int color) {
        this.fillColor = color;
        return this;
    }

    public Discrete withStroke(int color, double weight) {
        this.strokeColor = color;
        this.strokeWeight = (float) weight;
        return this;
    }


    public interface Function<T> {
        T[] apply(T[] lastValues, T[] secondLastValues, double xMin, double xMax, double deltaSeconds);
    }

    public interface Neighbor<T> extends Function<T> {

        @Override
        default T[] apply(T[] lastValues, T[] secondLastValues, double xMin, double xMax, double deltaSeconds) {
            // Returns an array with the first and the last value as in the oldValues
            // and at any other position with new values
            T[] newValues = lastValues.clone();
            double deltaX = (xMax - xMin) / (lastValues.length - 1);

            IntStream.range(1, lastValues.length - 1).forEach(i ->
                    newValues[i] = applySingle(
                            lastValues[i - 1],
                            lastValues[i],
                            lastValues[i + 1],
                            secondLastValues[i],
                            xMin + i * deltaX,
                            deltaSeconds));

            return newValues;
        }

        T applySingle(T lastLeft, T last, T lastRight, T secondLast, double xPos, double deltaSeconds);
    }

    public interface ColorMapper<T> {

        int mapToColor(T value);

    }
}
