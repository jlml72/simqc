package kotlinMath

import kotlin.math.pow


data class Matrix(val m: Int, val n: Int, val values: Array<Complex>): Iterable<Complex> {

    init {
        require(m > 0 && n > 0) { "Die Matrix darf nicht leer sein!" }
    }

    constructor(m: Int, n: Int): this(m, n, Array(m*n) { 0 + 0*î })

    constructor(rows: Array<Array<Number>>): this(rows.size, rows[0].size, rows.flatten().map { n -> n.toComplex() }.toTypedArray()) {
        require(rows.all { row -> row.size == rows[0].size }) {"Die Matrix muss in jeder Zeile die gleiche Menge an Einträgen besitzen!"}
    }

    private fun rowOf(index: Int) = Math.floorMod(index / n, m)
    private fun columnOf(index: Int) = index % n
    private fun size() = m * n;

    operator fun get(i: Int) = values[Math.floorMod(i, size())]
    operator fun get(i: Int, j: Int) = values[Math.floorMod(i, m) * n + Math.floorMod(j, n)]

    private fun getRow(index: Int) = values.sliceArray(index * n until (index + 1) * n)
    private fun getColumn(index: Int) = values.indices.filter { i -> i % n == index }.map(values::get).toTypedArray()
    private fun getRows() = Array(m, this::getRow)
    private fun getCols() = Array(n, this::getColumn)

    operator fun unaryMinus() = Matrix(m, n, values.map { v -> -v }.toTypedArray())
    operator fun times(other: Complex) = Matrix(m, n, values.map { z -> z * other }.toTypedArray())
    operator fun times(other: Number) = Matrix(m, n, values.map { z -> z * other.toDouble() }.toTypedArray())

    operator fun plus(other: Matrix) {
        require(m == other.m && n == other.n) { "Die Matrizen müssen die gleiche Größe für Subtraktion haben" }
        Matrix(m, n, this.zip(other).map { p -> p.first + p.second }.toTypedArray())
    }
    operator fun minus(other: Matrix) {
        require(m == other.m && n == other.n) { "Die Matrizen müssen die gleiche Größe für Subtraktion haben" }
        Matrix(m, n, this.zip(other).map { p -> p.first - p.second }.toTypedArray())
    }

    /*operator fun times(other: Matrix): Matrix {

        return Matrix(m, other.n,
            values.indices
            .map { i -> (0 until n).map { j -> this[rowOf(i), j] * other[j, columnOf(i)] }.sum() }
            .toTypedArray())
    }*/

    operator fun times(other: Matrix): Matrix {
        require(this.n == other.m) { "Die Größen der zwei Matrizen ist nicht für die Multiplikation kompatibel!" }
        return Matrix(Array(m) { row ->
            Array(other.n) { col ->
                getRow(row).zip(other.getColumn(col)).map { p -> p.first * p.second }.sum()
            }
        })
    }


    infix fun kronzeptionell(other: Matrix): Matrix {
        val rows = other.getRows().map { row -> row.map { z -> z * this }}

        val newM = m * other.m
        val newN = n * other.n

        //rows.map { row -> Array() }



        return Matrix(arrayOf(arrayOf()));

    }



    infix fun 𝓴𝓻𝓸𝓷(other: Matrix): Matrix {
        val nNew = n * other.n
        val mNew = m * other.m
        val array = Array(nNew * mNew) { ZERO }
        for (i1 in 0 until m)
            for (j1 in 0 until n)
                for (i2 in 0 until other.m)
                    for (j2 in 0 until other.n)
                        array[(i1 * other.m + i2) * nNew + j1 * other.n + j2] = this[i1, j1] * other[i2, j2]
        return Matrix(nNew, mNew, array)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Matrix

        if (m != other.m) return false
        if (n != other.n) return false
        if (!values.contentEquals(other.values)) return false

        return true
    }
    override fun hashCode(): Int {
        var result = m
        result = 31 * result + n.hashCode()
        result = 31 * result + values.contentHashCode()
        return result
    }

    override fun iterator(): Iterator<Complex> = values.iterator()

    override fun toString(): String {
        var string = "Matrix: [\n";

        for (i in 0 until m) {
            string += "        ["
            string += this[i, 0]
            for (j in 1 until n)
                string +=  ", " + this[i, j]
            string += "]\n"
        }

        string += "]"

        return string;
    }

}

operator fun Number.times(other: Matrix) = other * this

fun eye(size: Int) = Matrix(size, size, (0 until size * size).map { i -> if (i % (size + 1) == 0) 1+0*î else 0*î }.toTypedArray())





fun main() {

    val not = Matrix(arrayOf(
        arrayOf(0, 1),
        arrayOf(1, 0)
    ))

    val h = 1 / 2.0.pow(0.5) * Matrix(arrayOf(
        arrayOf(1,  1),
        arrayOf(1, -1)
    ))

    println(not * h)
}