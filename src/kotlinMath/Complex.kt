package kotlinMath

import kotlin.math.*

/*
ALGEBRAIC:
a: real
b: imag

POLAR:
a: r
b: phi
 */

val EPSILON = 1e-12

data class Complex(val a: Double, val b: Double, val form: Form) : Number() {

    // Form
    enum class Form { ALGEBRAIC, POLAR }

    // Cast to different form
    private fun algebraic(): Complex {
        if (form == Form.ALGEBRAIC) return this
        val real: Double = a * cos(b)
        val imag: Double = a * sin(b)
        return algebraic(real, imag)
    }

    private fun polar(): Complex {
        if (form == Form.POLAR) return this
        val r: Double = sqrt(a * a + b * b)
        val phi: Double = atan2(b, a)
        return polar(r, phi)
    }

    constructor(a: Number, b: Number, form: Form): this(a.toDouble(), b.toDouble(), form)

    fun real(): Double = algebraic().a
    fun imag(): Double = algebraic().b
    fun r(): Double = polar().a
    fun phi(): Double = polar().b


    operator fun unaryPlus() = algebraic(real(), imag())
    operator fun unaryMinus() = polar(-real(), -imag())

    // Complex <operator> Complex
    operator fun plus(other: Complex)  = algebraic(real() + other.real(), imag() + other.imag())
    operator fun minus(other: Complex) = algebraic(real() - other.real(), imag() - other.imag())
    operator fun times(other: Complex) = polar(r() * other.r(), phi() + other.phi())
    operator fun div(other: Complex)   = polar(r() / other.r(), phi() - other.phi())

    // Complex <operator> Number
    operator fun plus(other: Number)  = this + algebraic(other.toDouble(), 0)
    operator fun minus(other: Number) = this - algebraic(other.toDouble(), 0)
    operator fun times(other: Number) = this * algebraic(other.toDouble(), 0)
    operator fun div(other: Number)   = this / algebraic(other.toDouble(), 0)

    override fun toByte(): Byte = r().toInt().toByte()
    override fun toChar(): Char = r().toInt().toChar()
    override fun toDouble(): Double = r()
    override fun toFloat(): Float = r().toFloat()
    override fun toInt(): Int = r().toInt()
    override fun toLong(): Long = r().toLong()
    override fun toShort(): Short = r().toInt().toShort()

    override fun toString(): String {
        return if (form === Form.ALGEBRAIC) {
            if (abs(b) < EPSILON) a.stringify()
            else if (abs(a) < EPSILON) "${b.stringify()}î"
            else if (b < 0) "${a.stringify()}${b.stringify()}î"
            else "${a.stringify()}+${b.stringify()}î"
        } else {
            //"$a*e^(${b}î)"
            algebraic().toString()
        }
    }


}

private fun Double.stringify(): String {
    return if (this == sqrt(2.0)) "√2"
    else if (this == -sqrt(2.0)) "-√2"
    else if (this == 1/sqrt(2.0) || this == 0.7071067811865474) "1/√2"
    else if (this == -1/sqrt(2.0) || this == -0.7071067811865475) "-1/√2"
    else if(this.toInt().toDouble() == this) this.toInt().toString()
    else this.toString()
}

// Custom constructor
fun algebraic(real: Number, imag: Number) : Complex = Complex(real, imag, Complex.Form.ALGEBRAIC)
fun polar(r: Number, phi: Number) : Complex = Complex(r, phi, Complex.Form.POLAR)

// Number <operator> Complex    Implies constructor: a + b * i
operator fun Number.plus(other: Complex)  = algebraic(this.toDouble(), 0) + other
operator fun Number.minus(other: Complex) = algebraic(this.toDouble(), 0) - other
operator fun Number.times(other: Complex) = algebraic(this.toDouble(), 0) * other
operator fun Number.div(other: Complex)   = algebraic(this.toDouble(), 0) / other

// Custom constructor for polar     (a e b * i)
infix fun Number.e(other: Complex) = polar(this.toDouble() * E.pow(other.real()), other.imag())

// Custom converter function
fun Number.toComplex() =  if (this is Complex) this else algebraic(this, 0)

// Constants
val î = algebraic(0, 1)
val π = PI
val ZERO = algebraic(0, 0)






// Don't look
fun Iterable<Complex>.sum(): Complex {
    var sum: Complex = ZERO
    for (element in this) {
        sum += element
    }
    return sum
}