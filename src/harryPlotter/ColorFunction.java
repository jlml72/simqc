package harryPlotter;

public interface ColorFunction {

    int apply(ComplexFast complex, int fractalDeepness);

    interface Simple extends ColorFunction {

        @Override
        default int apply(ComplexFast complex, int fractalDeepness) {
            return applySimple(complex);
        }

        int applySimple(ComplexFast complex);

    }

}
