package kotlinMath

import java.util.DoubleSummaryStatistics
import kotlin.math.log
import kotlin.math.pow

fun Matrix.numBits(): Int {
    require(this.m == this.n || this.n == 1) // Größe muss quadratisch oder ein Vektor (1 Spalte) sein
    val numBits = log(this.m.toDouble(), 2.0)
    require(numBits == numBits.toInt().toDouble()) // Größe muss ein Faktor von 2 sein
    return numBits.toInt()
}

data class QuantumState(val bitstates: Matrix) {

    constructor(n: Int): this(Matrix(2.0.pow(n).toInt(), 1)) {
        bitstates.values[0] = 1 + 0*î
    }

    fun op(operator: Matrix, i: Int = 0):QuantumState {
        val eyeL = eye(2.0.pow(i).toInt())
        val eyeR = eye(2.0.pow(bitstates.numBits() - i - operator.numBits()).toInt())
        return QuantumState((eyeL 𝓴𝓻𝓸𝓷 operator 𝓴𝓻𝓸𝓷 eyeR) * bitstates)
    }

    fun show(): QuantumState {
        println(toString())
        return this
    }

    override fun toString(): String {
        var string = "QState: [\n"

        for (i in 0 until bitstates.m) {
            string += "    "
            string += String.format("%" + log(bitstates.m.toDouble(), 2.0).toInt() + "s", i.toString(2)).replace(" ".toRegex(), "0")
            string += ": " + bitstates[i, 0] + "\n"
        }

        string += "]"

        return string;
    }

}

val NOT = Matrix(arrayOf(
    arrayOf(0, 1),
    arrayOf(1, 0)
))

val AND = Matrix(arrayOf(
    arrayOf(1, 0, 1, 0),
    arrayOf(0, 1, 0, 0),
    arrayOf(0, 0, 0, 0),
    arrayOf(0, 0, 0, 1)
))

val SWAP = Matrix(arrayOf(
    arrayOf(1, 0, 0, 0),
    arrayOf(0, 0, 1, 0),
    arrayOf(0, 1, 0, 0),
    arrayOf(0, 0, 0, 1)
))

val CNOT = Matrix(arrayOf(
    arrayOf(1, 0, 0, 0),
    arrayOf(0, 1, 0, 0),
    arrayOf(0, 0, 0, 1),
    arrayOf(0, 0, 1, 0)
))

fun main() {

    var state = QuantumState(4)

    state.show().op(NOT, 1).show().op(SWAP).show()


}




